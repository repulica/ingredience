package repulica.separateingredients.mixin;

import repulica.separateingredients.IngredientManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

@Mixin(AnvilScreenHandler.class)
public class MixinAnvilScreenHandler {
	@Redirect(method = "updateResult", at = @At(value = "INVOKE", target = "Lnet/minecraft/item/Item;canRepair(Lnet/minecraft/item/ItemStack;Lnet/minecraft/item/ItemStack;)Z"))
	private boolean injectCanRepair(Item item, ItemStack stack, ItemStack ingredient) {
		Identifier itemId = Registry.ITEM.getId(item);
		Identifier repairId = new Identifier(itemId.getNamespace(), "repair/" + itemId.getPath());
		if (IngredientManager.INSTANCE.hasIngredient(repairId)) {
			Ingredient ing = IngredientManager.INSTANCE.getIngredient(repairId);
			return ing.test(ingredient);
		}
		return item.canRepair(stack, ingredient);
	}
}
