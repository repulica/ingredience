package repulica.separateingredients.mixin;

import java.util.Map;

import com.google.gson.JsonElement;
import repulica.separateingredients.IngredientManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.recipe.RecipeManager;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.profiler.Profiler;

@Mixin(RecipeManager.class)
public class MixinRecipeManager {
	@Inject(method = "apply", at = @At("HEAD"))
	private void refreshIngredientManager(Map<Identifier, JsonElement> json, ResourceManager resourceManager, Profiler profiler, CallbackInfo info) {
		IngredientManager.INSTANCE.reload(resourceManager);
	}
}
