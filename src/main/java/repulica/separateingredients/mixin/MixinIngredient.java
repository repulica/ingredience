package repulica.separateingredients.mixin;

import com.google.gson.JsonObject;
import repulica.separateingredients.IngredientEntry;
import repulica.separateingredients.IngredientManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.recipe.Ingredient;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;

@Mixin(Ingredient.class)
public class MixinIngredient {

	@Inject(method = "entryFromJson", at = @At("HEAD"), cancellable = true)
	private static void injectReferenceIngredient(JsonObject json, CallbackInfoReturnable<Ingredient.Entry> info) {
		if (json.has("ingredient")) {
			Identifier id = new Identifier(JsonHelper.getString(json, "ingredient"));
			info.setReturnValue(new IngredientEntry(id, IngredientManager.INSTANCE.getIngredient(id)));
		}
	}
}
