package repulica.separateingredients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraft.recipe.Ingredient;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;

public class IngredientManager {
	public static final IngredientManager INSTANCE = new IngredientManager();
	private static final Logger LOGGER = LogManager.getLogger();
	private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
	private static final int PREFIX_LENGTH = "ingredients/".length();
	private static final int SUFFIX_LENGTH = ".json".length();

	private final Map<Identifier, Ingredient> ingredients = new HashMap<>();
	private boolean loaded = false;

	public void reload(ResourceManager manager) {
		loaded = false;
		ingredients.clear();
		Collection<Identifier> resources = manager.findResources("ingredients", name -> name.endsWith(".json"));
		for (Identifier id : resources) {
			try {
				Identifier newId = new Identifier(id.getNamespace(), id.getPath().substring(PREFIX_LENGTH, id.getPath().length() - SUFFIX_LENGTH));
				Reader reader = new BufferedReader(new InputStreamReader(manager.getResource(id).getInputStream(), StandardCharsets.UTF_8));
				JsonElement json = JsonHelper.deserialize(GSON, reader, JsonElement.class);
				ingredients.put(newId, Ingredient.fromJson(json));
			} catch (IOException e) {
				LOGGER.error("Could not load ingredient {}: {}", id.toString(), e.getMessage());
				e.printStackTrace();
			}
		}
		loaded = true;
		LOGGER.info("Loaded {} ingredients", ingredients.keySet().size());
	}

	public Ingredient getIngredient(Identifier id) {
		if (!loaded) {
			LOGGER.error("Ingredient {} was searched for before ingredients were loaded!", id.toString());
			return Ingredient.EMPTY;
		}
		return ingredients.getOrDefault(id, Ingredient.EMPTY);
	}

	public boolean hasIngredient(Identifier id) {
		if (!loaded) {
			LOGGER.error("Ingredient {} was searched for before ingredients were loaded!", id.toString());
			return false;
		}
		return ingredients.containsKey(id);
	}
}
