package repulica.separateingredients;

import java.util.Arrays;
import java.util.Collection;

import com.google.gson.JsonObject;
import repulica.separateingredients.mixin.IngredientAccessor;

import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.Identifier;

public class IngredientEntry implements Ingredient.Entry {
	private final Identifier id;
	private final ItemStack[] entries;

	public IngredientEntry(Identifier id, Ingredient ingredient) {
		this.id = id;
		((IngredientAccessor) (Object) ingredient).invokeCacheMatchingStacks();
		this.entries = ((IngredientAccessor) (Object) ingredient).getMatchingStacks();
	}

	@Override
	public Collection<ItemStack> getStacks() {
		return Arrays.asList(entries);
	}

	@Override
	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		json.addProperty("ingredient", id.toString());
		return json;
	}
}
