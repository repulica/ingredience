# separate ingredients

allows you to define ingredients in the `data/<namespace>/ingredients` folder.

uses the standard vanilla ingredient format.
 
ingredients defined there can be referred to in recipes or used as tool repair materials.
 
to reference an ingredient in a recipe, add the ingredient as `{"ingredient":"<ingredient name>"}` in the recipe json.
 
to modify the repair material of a tool, place the ingredient json at `data/<tool namespace>/repair</tool name>.json`.
 
if no repair material json is defined, the default repair material for the tool will be used.
 
mods which want to be able to use defined ingredients can call `IngredientManager.INSTANCE.hasIngredient(Identifier id)`
to query if a specifically-named ingredient exists and`IngredientManager.INSTANCE.getIngredient(Identifier id)` to get
the ingredient itself. if no ingredient with that name exists, it will return an empty ingredient.
